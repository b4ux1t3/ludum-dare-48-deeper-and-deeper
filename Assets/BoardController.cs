using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{

    public GameObject[] Spaces;

    private int occupiedSpaces;
    // Start is called before the first frame update
    void Awake()
    {
        occupiedSpaces = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool InsertEnemy(EnemyGameObject enemy)
    {
        if (occupiedSpaces >= Spaces.Length) return false;

        enemy.transform.SetParent(Spaces[occupiedSpaces++].transform, false);
        return true;
    }

}
