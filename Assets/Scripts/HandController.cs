using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public CardGameObject cardPrefab;
    public DiscardController discardController;
    public GameObject[] CardSlots;
    public CardGameObject[] Cards;
    public DeckController deckController;
    private Vector2 resetVector;
    private int MaxHandSize;
    void Awake()
    {
        MaxHandSize = CardSlots.Length;
        Debug.Log(MaxHandSize);
        Cards = new CardGameObject[MaxHandSize];
        deckController = FindObjectOfType<DeckController>();
        resetVector = new Vector2(0, 0);
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildHand()
    {
        for (int i = 0; i < this.MaxHandSize; i++)
        {
            if (Cards[i] == null)
            {
                Cards[i] = deckController.DrawCard();

                Cards[i].transform.SetParent(CardSlots[i].transform);
                Cards[i].transform.position = transform.parent.position;
                Cards[i].transform.localPosition = resetVector;
            }
        }    
    }

    internal void Remove(CardGameObject card)
    {
        var indexToRemove = FindCard(card);
        if (indexToRemove == -1) throw new ArgumentOutOfRangeException();

        
        Cards[indexToRemove] = null;
    }

    private int FindCard(CardGameObject card)
    {
        for (int i = 0; i < MaxHandSize; i++)
        { 
            if (Cards[i] == card) return i;
        }
        return -1;
    }
}
