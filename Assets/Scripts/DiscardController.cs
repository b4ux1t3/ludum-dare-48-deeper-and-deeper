using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardController : MonoBehaviour
{
    public List<CardGameObject> Cards;
    private GameManager gameManager;
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        Cards = new List<CardGameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Discard(CardGameObject card)
    {
        Cards.Add(card);
        card.transform.SetParent(transform, false);
    }

    internal List<CardGameObject> GetCards()
    {
        List<CardGameObject> cards = new List<CardGameObject>(Cards);
        Cards.Clear();
        return cards;
    }
}
