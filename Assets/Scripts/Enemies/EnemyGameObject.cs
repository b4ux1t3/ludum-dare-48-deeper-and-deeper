using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Enemy;

public class EnemyGameObject : MonoBehaviour
{
    public EnemyName Name;
    public EnemyType Type;
    public Image artwork;
    public GameManager gameManager;
    public int health;
    
    void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // Maybe show what enemies a particular card _could_ target.
    public void Highlight()
    {

    }

    internal void Die()
    {
        this.gameObject.SetActive(false);
    }

    internal void UpdateData(EnemyController.EnemyDetails details)
    {
        this.artwork.sprite = details.Artwork;
        this.health  = details.Health;
    }
}
