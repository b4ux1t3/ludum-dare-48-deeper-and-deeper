using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Enemy;
using System;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
#region Fields
    public List<EnemyGameObject> Enemies;
    public EnemyGameObject EnemyPrefab;
    public BoardController Board;
    private Dictionary<EnemyName, EnemyType> TypeLookup;
    private Dictionary<EnemyName, Sprite> ImageLookup;
    public Sprite UndergrowthImage;
#endregion
#region Unity Stuff
    void Awake()
    {
        Enemies = new List<EnemyGameObject>();
        TypeLookup = new Dictionary<EnemyName, EnemyType>();
        ImageLookup = new Dictionary<EnemyName, Sprite>();
        BuildTypeDictionary();
        BuildImageDictionary();
        MakeEnemies();
        
    }
#endregion

#region Enemy Management
    private void MakeEnemies()
    {
        for (int i = 0; i < 15; i++) Enemies.Add(Instantiate<EnemyGameObject>(EnemyPrefab, Board.transform, false));
        UpdateEnemies();
        PlaceEnemies();
    }
    private EnemyGameObject[] FindTargets(EnemyName enemyName)
    {
        var potentialTargets = Enemies.Where(item => item.Name == enemyName).ToArray();

        if (potentialTargets.Count() == 0)
        {
            return null;
        }

        return potentialTargets;
    }

    private void UpdateEnemies()
    {
        var undergrowths = Enemies.Where(item => item.Name is EnemyName.Undergrowth).ToArray();
        foreach (var undergrowth in undergrowths)
        {
            undergrowth.UpdateData(new EnemyDetails(1, UndergrowthImage));
        }
    }

    private void PlaceEnemies()
    {
        foreach (var enemy in Enemies)
        {
            if (!Board.InsertEnemy(enemy)) Enemies.Remove(enemy);
        }
    }
#endregion

#region Dictionary Builders
    private void BuildTypeDictionary()
    {
        TypeLookup.Add(EnemyName.Undergrowth, EnemyType.Static);
    }
    private void BuildImageDictionary()
    {
        ImageLookup.Add(EnemyName.Undergrowth, UndergrowthImage);
    }
#endregion

#region Card Handlers
    internal bool ClearUnderGrowth()
    {
        
        var potentialTargets = FindTargets(EnemyName.Undergrowth);

        if (potentialTargets == null)
        {
            return false;
        }

        int indexToKill = UnityEngine.Random.Range(0,potentialTargets.Count() - 1);
        Enemies.Remove(potentialTargets[indexToKill]);
        potentialTargets[indexToKill].Die();
        return true;
    }

    
    internal bool Investigate()
    {
        var potentialTargets = FindTargets(EnemyName.Undergrowth);

        if (potentialTargets == null)
        {
            return false;
        }

        int indexToReveal = UnityEngine.Random.Range(0,potentialTargets.Count() - 1);

        Enemies.Remove(potentialTargets[indexToReveal]);
        // Enemies.Add(potentialTargets[indexToReveal].Reveal());
        return true;

    }
#endregion

    /// JANKY SHIT ///
    public struct EnemyDetails
    {
        public EnemyDetails(int health, Sprite artwork)
        {
            Health = health;
            Artwork = artwork;
        }

        public int Health;
        public Sprite Artwork;
        
    }

    
}
