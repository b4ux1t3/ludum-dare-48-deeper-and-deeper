using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardGameObject : MonoBehaviour
{
    public string Name;
    public string FlavorText;
    public int Cost;
    public TMP_Text NameText;
    public TMP_Text CostText;
    public Image ArtworkImage;
    private GameManager gameManager;
    public Vector2 ScaleSize;
    public Vector3 ShiftUpAmount;
    void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void Play()
    {
        gameManager.PlayCard(this);
    }
    public void HoverStart()
    {
        transform.localScale    *= ScaleSize;
        transform.localPosition += ShiftUpAmount;
    }
    public void HoverEnd()
    {
        transform.localScale    /= ScaleSize;
        transform.localPosition -= ShiftUpAmount;
    }

    internal void UpdateDetails(GameManager.CardDetails cardDetails)
    {
        this.Name       = cardDetails.Name;
        this.Cost       = cardDetails.Cost;
        this.FlavorText = cardDetails.FlavorText;

        NameText.SetText(Name);
        CostText.SetText(Cost.ToString());
    }
}
