using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
#region Variables
    public DeckController deckController;
    public DiscardController discardController; 
    public static Dictionary<string, Func<bool>> cardEffectLookup;
    public CardGameObject cardPrefab;
    public HandController PlayerHand;
    public EnemyController enemyController;
    // Add all cards which are instantiated in the deck to this List
    public GameObject[] QueueAreas;
    public int CardsPlayedThisTurn { get; private set; }
    private List<CardGameObject> ActiveCards;
    private Dictionary<string, CardDetails> CardLookup;

    // When you shuffle the deck during play (DURING PLAY! Not between stages), move one of the cards here instead.
    private List<CardGameObject> BurnedCards;
    private Queue<CardGameObject> PlayQueue;

    #endregion
    void Awake()
    {
        // First we need to build our dictionary by defining delegate functions _somewhere_
        this.BuildEffectDictionary();
        this.BuildCardDetailsDictionary();
        // Also, we have to instantiate our FREAKING LISTS.
        ActiveCards = new List<CardGameObject>();
        BurnedCards = new List<CardGameObject>();
        PlayQueue = new Queue<CardGameObject>();
    }

    private void BuildCardDetailsDictionary()
    {
        CardLookup = new Dictionary<string, CardDetails>();

        CardLookup.Add("Clear Undergrowth", new CardDetails(1, "Clear Undergrowth", "Slash!"));
    }

    void Start()
    {
        PlayerHand.BuildHand();
    }
    private void BuildEffectDictionary(){
        cardEffectLookup = new Dictionary<string, Func<bool>>();

        cardEffectLookup.Add("Clear Undergrowth", new Func<bool>(() => {
            return enemyController.ClearUnderGrowth();
        }));

        cardEffectLookup.Add("Dummy Card", new Func<bool>(() => {
            Debug.Log("Dummy card played!");
            return true;
        }));

        cardEffectLookup.Add("Investigate", new Func<bool>(() => {
            return enemyController.Investigate();
        }));
    }

    
    internal void PlayCard(CardGameObject card)
    {
        CardsPlayedThisTurn += 1;
        if (!PlayQueue.Contains(card)) // if the card isn't in our queue. . .
        {
            try
            {
                PlayerHand.Remove(card);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.Log(ex);
            }
            card.transform.SetParent(QueueAreas[CardsPlayedThisTurn - 1].transform, false);
            PlayQueue.Enqueue(card);
        }
    }
    internal Stack<CardGameObject> ShuffleDeck()
    {
        System.Random rand = new System.Random();
        // Get our cards from the discardController.
        var cards = discardController.GetCards();
        // Add one of those cards to the burn pile
        var index = rand.Next(cards.Count);

        BurnedCards.Add(cards[index]);
        cards[index].transform.SetParent(transform, false);
        cards.RemoveAt(index);

        var shuffledArray = new Stack<CardGameObject>(cards.OrderBy(x => rand.Next()));

        return shuffledArray;
    }

    public CardGameObject GenerateCard(string name)
    {
        var newCard = Instantiate<CardGameObject>(cardPrefab, deckController.transform);
        var cardDetails = CardLookup[name];
        newCard.UpdateDetails(cardDetails);
        ActiveCards.Add(newCard);
        return newCard;
    }
    public void EndTurn()
    {
        while (PlayQueue.Count > 0)
        {
            var currentCard = PlayQueue.Dequeue();
            var success = cardEffectLookup[currentCard.Name]();
            discardController.Discard(currentCard);
            if (success) Debug.Log($"{currentCard.Name} worked!");
            else Debug.Log($"{currentCard.Name} didn't work!");
        }
        CardsPlayedThisTurn = 0;
        // Enemy stuff

        // Rebuild player hand
        PlayerHand.BuildHand();
    }

    public struct CardDetails
    {
        public CardDetails(int cost, string name, string flavorText)
        {
            Cost = cost;
            Name = name;
            FlavorText = flavorText;
        }
        public int Cost;
        public string Name;
        public string FlavorText;
    }
}
