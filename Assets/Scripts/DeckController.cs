using System;
using System.Collections.Generic;
using UnityEngine;

public class DeckController : MonoBehaviour
{
    protected Stack<CardGameObject> Cards;
    public int DeckSize;
    private GameManager gameManager;

    
    public void Awake()
    {
        Cards = new Stack<CardGameObject>();
        gameManager = FindObjectOfType<GameManager>();
        // When we start, we want to generate a deck of cards from a list somewhere.
        // For now, we'll just debug.log that we were created.
        Debug.Log("DeckController is up!");

        for (int i = 0; i < DeckSize; i++) Cards.Push(gameManager.GenerateCard("Clear Undergrowth"));
    }

    public CardGameObject DrawCard()
    {
        var drawnCard = Cards.Pop();
        if(Cards.Count == 0) ShuffleDeck();
        return drawnCard;
    }

    private void ShuffleDeck()
    {
        Cards = gameManager.ShuffleDeck();
        foreach (var card in Cards)
        {
            card.transform.SetParent(transform, false);
        }
    }
}
